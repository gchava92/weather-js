import "./style.css";

export const WeatherHeader = (props) => {
  //getting the first (current weather)
  const icon = props[0]?.icon;
  const temp = Number(props[0]?.temp || 0).toFixed(0);

  setInterval(function () {
    const utc = window.citySelected?.utc ?? "America/Managua";

    const currentTime = new Date().toLocaleTimeString("en-US", {
      timeZone: utc,
    });

    document.querySelector("#currentTime").textContent = `${currentTime}`;
  }, 990);

  return `
    <article class="weather__header">
        <div class="weather__header__details">
            <img width="72" src="${`http://openweathermap.org/img/wn/${icon}@2x.png`}" />
        
            <h6>${props[0]?.weather || ""}</h6>
        </div>

        <div>
            <h2>
                ${temp}°C
            </h2>
            <p id="currentTime">00:00:00 AM</p>
        </div>        
    </article>
    `;
};
