import "./style.css";

import { CityModel } from "../../utils/CityModel";

export const Cities = `
<article class="cities__main">
    <div class="cities__group__buttons">
    ${CityModel.map((city) => {
      return `
      <button id="${city.name}" class="cities__button">
        ${city.name}
      </button>`;
    })
      .toString()
      .replaceAll(",", "")}
        
    </div>
</article>
`;
