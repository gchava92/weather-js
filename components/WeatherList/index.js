import "./style.css";

export const WeatherList = (props) => {
  let list = "";

  (props || []).forEach((day) => {
    list += `
    <li>
        <span>${day.datetime}</span>
        <span>${Number(day?.temp || 0)} °C</span>

        <span>
            <img width="32" src="${`http://openweathermap.org/img/wn/${day.icon}@2x.png`}" />
        </span>

        <span>
            ${day.weather}
        </span>
    </li>
    `;
  });

  return `
    <article class="weather__list" >
        <ul>
           ${list}
        </ul>
    </article>
    `;
};
