import "./style.css";

import { Cities } from "../Cities";
import { WeatherHeader } from "../WeatherHeader";
import { WeatherList } from "../WeatherList";

import { getWeather } from "../../services/openWeather";
import { CityModel } from "../../utils/CityModel";
import { WeatherModel } from "../../utils/WeatherModel";

import { getBackground } from "../../utils/getBackground";

export const WeatherMain = `
    <div class="weather__button__area">
        <button class="weather__button__lang" title="Change Language">en</button>
    </div>

    <div id="weather__main"></div>
`;

export const renderWeatherMain = async () => {
  const currentCity = window.citySelected?.name ?? CityModel[0].name;
  const currentLang = document.querySelector(
    ".weather__button__lang"
  ).textContent;

  //getting info from api
  const response = await getWeather({
    lat: window.citySelected?.lat ?? CityModel[0].lat,
    lon: window.citySelected?.lon ?? CityModel[0].lon,
    lang: currentLang,
  });

  //re-organize info using my model
  const model = WeatherModel(response);
  console.log({ response, model });

  //rendering weather box
  document.querySelector("#weather__main").innerHTML = `
    ${WeatherHeader(model)}

    <section class="weather__section"> 
        ${Cities}

        ${WeatherList(model)}
    </section>
    `;

  //change button status
  document
    .querySelector(`#${currentCity}`)
    .setAttribute("class", "cities__button cities__button__active");

  //change background
  const bg = getBackground(currentCity);

  console.log({ bg, currentCity });

  document
    .querySelector("body")
    .setAttribute("class", `bg__image__common ${bg}`);

  document.querySelectorAll(".cities__button").forEach((node) => {
    node.addEventListener("click", async (e) => {
      console.log("change city to: ", e.target.id);
      window.citySelected = CityModel.find(({ name }) => name === e.target.id);

      await renderWeatherMain();
    });
  });
};

console.log({ WeatherMain });

document.addEventListener("DOMContentLoaded", async function () {
  document
    .querySelector(".weather__button__lang")
    .addEventListener("click", async function (e) {
      console.log(e.target.textContent);
      if (e.target.textContent === "en") {
        document.querySelector(".weather__button__lang").textContent = "es";
      } else {
        document.querySelector(".weather__button__lang").textContent = "en";
      }

      await renderWeatherMain();
    });
  //render
  await renderWeatherMain();
});
