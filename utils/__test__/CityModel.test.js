import { CityModel } from "../CityModel";

test("should be a city", () => {
  const { name } = CityModel.find(({ name }) => name === "Managua");

  expect(name).toBe("Managua");
});
