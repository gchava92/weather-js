export const CityModel = [
  {
    name: "Managua",
    lat: "12.129007171089317",
    lon: "-86.26542715058413",
    utc: "America/Managua",
  },
  {
    name: "Madrid",
    lat: "40.41670459460082",
    lon: "-3.7040969189573807",
    utc: "Europe/Madrid",
  },
  {
    name: "Tokyo",
    lat: "35.67727362117068",
    lon: "139.76690882365457",
    utc: "Asia/Tokyo",
  },
];
