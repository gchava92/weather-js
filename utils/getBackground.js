import { CityModel } from "./CityModel";

export const getBackground = (currentCity) => {
  const { utc } = CityModel.find(({ name }) => name === currentCity);

  const [_hour] = new Date()
    //british use 24 hours
    .toLocaleTimeString("en-GB", {
      timeZone: utc,
    })
    .split(":");

  const hour = Number(_hour);

  let bg = "bg__image__04";

  if (hour > 5 && hour < 15) {
    bg = "bg__image__01";
  } else if (hour >= 15 && hour < 18) {
    bg = "bg__image__03";
  }

  console.log({ bg, _hour, utc });

  return bg;
};
