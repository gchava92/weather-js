### APP Meteorológica

[![Netlify Status](https://api.netlify.com/api/v1/badges/68147f10-8630-43a5-aa90-9305ff3ae389/deploy-status)](https://app.netlify.com/sites/weather-js-2022/deploys)

#### Tecnologias
> HTML, CSS 3, Javascript

#### Initialize el project.
> Este project usa vite, para el manejo de paquetes y compilación.
> Instalar paquetes con yarn ó npm install
> Ejecutar: Yarn dev ó npm run dev


#### CSS
> Utilize el sistema BEM para evitar colapso entre las clases CSS, ya que no hay un "generador" de clases dinamicas (como por ejemplo css module)


#### Javascript.
> Decidi no utilizar Handlebar, en cambio estoy utilizando los templates strings de CSS ``.
> Nota: Si, Revise Handlebar, es muy parecido a Blade de Laravel. 


> Separe los elementos en components (para ayudarme a depurar)
> No use Jquery, porque considero que javascript ES6 es una buena alternativa (ademas de nativa).

#### Entry point
> El "component" mas importate seria el de components/Weather, es el que engloba a todos y tiene mayor logica.

#### Librerias.
> data-fns: En vez de moment.js, porque tiene tree-shacking (menor costo kb)
> testing-libray/dom: Para realizar pruebas unitarias.

### Features:
> Cambio de idiomas (EN/ES)
> Ver Clima en 3 ciudades.
> Clima actual en la ciudad seleccionada.
> Cambio de background en base al tiempo de la ciudad seleccionada.

#### Deploy.
> Subi una copia del project a Netlify: [Netlify Link](https://weather-js-2022.netlify.app/)

#### Test
> Cypress yarn cypress:open

<img width="400" alt="CleanShot 01" src="https://gitlab.com/gchava92/weather-js/uploads/00f33e40ffeebbd64486797785d4a473/CleanShot_2022-09-24_at_00.42.49_2x.png">


#### Preview. (screen shots)


<img width="400" alt="CleanShot 01" src="https://gitlab.com/gchava92/weather-js/uploads/80aec0010b0ebeeb8881651307210d2e/CleanShot_2022-09-23_at_03.41.26.png">


<img width="400" alt="CleanShot 02" src="https://gitlab.com/gchava92/weather-js/uploads/57c543e64f60b98f524e41b1d1f3091b/CleanShot_2022-09-23_at_03.45.06.png">
