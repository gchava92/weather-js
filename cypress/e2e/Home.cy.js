describe("Visit Home", () => {
  it("passes", () => {
    cy.visit("http://127.0.0.1:5173/");
    cy.contains("Managua");
    cy.get("#Managua");

    cy.get("#Tokyo").click();
  });
});

describe("Go to Tokyo", () => {
  it("passes", () => {
    cy.visit("http://127.0.0.1:5173/");

    cy.get("#Tokyo").click();
  });
});

describe("Change Language", () => {
  it("passes", () => {
    cy.visit("http://127.0.0.1:5173/");

    cy.get(".weather__button__lang").click();
    cy.contains("es");
  });
});
