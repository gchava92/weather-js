//openweathermap
const url = new URL("https://api.openweathermap.org/data/2.5/forecast");
const appid = "842a213d2ee4489d3bbd70b7f778fd0d";

url.searchParams.set("appid", appid);
url.searchParams.set("units", "metric");

export const getWeather = async ({ lat, lon, lang }) => {
  url.searchParams.set("lat", lat);
  url.searchParams.set("lon", lon);
  url.searchParams.set("lang", lang);

  const res = await window.fetch(url.toString());

  return res.json();
};
